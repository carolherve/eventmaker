import React, { Component } from 'react';
import {
  ScrollView,
  View,
  TextInput
} from 'react-native'
import { connect } from 'react-redux'
import EventRow from './eventRow'
import { eventsFetch } from '../actions/eventActions'
import _ from 'lodash'
import {SearchBar } from 'react-native-elements'

class EventsList extends Component {

  constructor(props){
    super(props)
    this.state ={
      searchTest: ''
    }
    eventsToShow = {}
  }

  componentWillMount(){
    this.props.eventsFetch()
  }

  setSearchText(event){
    this.setState({
      searchTest: event.nativeEvent.text.toLowerCase()
    })
  }

  filterEvents(searchText, events) {
    let text = searchText;

    return _.filter(events, (n) => {
      let event = n.type.toLowerCase();
      return event.search(text) !== -1;
    });
  }

  render() {
    return (
      <View>
        <SearchBar
          lightTheme
          cancelButtonTitle="Cancel"
          onChangeText={(text)=>this.setState({searchTest:text.toLowerCase()})}
          icon={{ type: 'font-awesome', name: 'search' }}
          placeholder='Type Here...'
        />
      <ScrollView>
        {this.filterEvents(this.state.searchTest, this.props.events).map(event => {
          return (
            <EventRow key={event.type} event={event}/>
          )
        })}
      </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const events = _.map(state.events, (val, uid) => {
    return { ...val, uid}
  })
  return { events }
}

export default connect( mapStateToProps, { eventsFetch })(EventsList)