/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'


export default class backButton extends Component {

  addEvent(){
    //open ton criss de menu
    Actions.pop()
  }
  render() {
    return (
      <TouchableOpacity onPress={this.addEvent}>
        <Icon
          name="arrow-back"
          color="white"
        />
      </TouchableOpacity>
    );
  }
}