/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'

export default class menuButton extends Component {

  openMenu(){
    //open ton criss de menu
  }
  render() {
    return (
     <TouchableOpacity onPress={this.openMenu}>
      <Icon
        name="menu"
        color="white"
      />
     </TouchableOpacity>
    );
  }
}
