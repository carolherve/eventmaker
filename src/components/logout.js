/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import firebase from 'firebase'


export default class Logout extends Component {

  addEvent(){
   firebase.auth().signOut()
  }
  render() {
    return (
      <TouchableOpacity onPress={this.addEvent}>
        <Icon
          name="add"
          color="white"
        />
      </TouchableOpacity>
    );
  }
}
