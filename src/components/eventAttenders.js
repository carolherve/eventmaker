import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import _ from 'lodash'
import {eventsPeople} from '../actions/eventAttendences'
import { connect } from 'react-redux'


class EventAttenders extends Component {
  state = { error: '', attending: false }


  componentWillMount() {
    this.props.eventsPeople({uid: this.props.event.uid})
  }


  render(){
    const { event } = this.props

    return (
      <ScrollView>
        <Text>People who are comming</Text>
        {this.props.attends.map(attendence => {
          return (
            <Text key={attendence.uid}>{attendence.email}</Text>
          )
        })}
      </ScrollView>
    )
  }
}
const styles = {

}

const mapStateToProps = (state) => {
  const attends = _.map(state.eventAttendence, (val, uid) => {
    return { ...val, uid}
  })
  console.log(attends)
  return { attends }

}
export default connect(mapStateToProps, {eventsPeople})(EventAttenders)