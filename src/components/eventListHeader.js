/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Header } from 'react-native-elements'
export default class navBar extends Component {
  render() {
    return (
      <Header
        centerComponent={{ text: 'SOCCER MONTREAL', style: { color: '#fff' } }}
      />
    );
  }
}
