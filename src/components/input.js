/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View
} from 'react-native';

export default class Input extends Component {
  state= { text: 'test' }

  render() {
    const { label, value, onChangeText, placeholder, secureTextEntry } = this.props
    return (
     <View style={styles.containerStyle}>
       <Text style= {styles.labelStyle}>{label}</Text>
       <TextInput
         autoCorrect={false}
         placeholder={placeholder}
         secureTextEntry={secureTextEntry}
         style={styles.inputStyle}
         value={value}
         onChangeText={onChangeText}
       />
     </View>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    lineHeight: 23,
    flex: 2,
  },
  labelStyle: {
    fontSize: 18,
    paddingLeft: 20,
    flex: 1
  },
  containerStyle: {
    height: 40,
    flex:1,
    flexDirection: 'row',
    alignItems: 'center'
  }
});
