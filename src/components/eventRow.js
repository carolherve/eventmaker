/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback
} from 'react-native';
import { Actions } from 'react-native-router-flux'

import EventsCardSection from './cardSection'


class EventRow extends Component {


  componentWillMount(){

  }

  onRowPress() {

    Actions.eventDetails({ event: this.props.event })
  }
  render() {
    const { event } = this.props
    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View style={styles.touchableStyle}>
        <EventsCardSection>
          <View style={styles.headerContentStyle}>
            <Text style={{ fontSize:16, fontWeight: 'bold'}}>{event.type} </Text>
            <Text>On: {event.date} </Text>
          </View>
          <View style={{ flex:1, alignItems: 'flex-end' }}>
            <Text>Attending: {event.confirmed}</Text>
            <Text>By: {event.owner}</Text>
          </View>
        </EventsCardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles =
  StyleSheet.create({
    headerContentStyle: {
      flexDirection: 'column',
      justifyContent: 'space-around',
      //alignItems: 'center',
      flex: 1
    },
    headerTextStyle: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    thumbnailStyle: {
      height: 50,
      width: 50
    },
    thumbnailContainerStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10,
      marginRight: 10
    },
    touchableStyle: {
      margin: 5,
    }
  });

export default EventRow