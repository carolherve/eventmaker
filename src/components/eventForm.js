import React, { Component } from 'react'
import { View, Text, Picker } from 'react-native'
import CardSection from './cardSection'
import Input from './input'
import { connect } from 'react-redux'
import { eventUpdate} from "../actions/eventActions"
import DatePicker from 'react-native-datepicker'
import {Form, InputField} from 'react-native-form-generator'

class EventForm extends Component {

 /* render() {
    return (
      <View>
       <CardSection>
                <Input
                  placeholder="Short description"
                  value={this.props.type}
                  onChangeText={text => this.props.eventUpdate({ prop: 'type', value: text })}
                />
       </CardSection>
        <CardSection>
          <Input
            placeholder="Where"
            value={this.props.place}
            onChangeText={text => this.props.eventUpdate({ prop: 'place', value: text })}
          />
        </CardSection>

        <CardSection>
          <DatePicker
            style={{width: 200}}
            date={this.props.date}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            minDate="2016-05-01"
            maxDate="2016-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36
              }
            }}
            onDateChange={(date) => {this.props.eventUpdate({prop: 'date', value: date})}}
          />
        </CardSection>

        <CardSection>
          <DatePicker
            style={{width: 200}}
            date={this.props.heure}
            mode="time"
            placeholder="select time"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"

            onDateChange={(time) => {this.props.eventUpdate({prop: 'heure', value: time})}}
          />
        </CardSection>



        <CardSection>
          <Input
            label="How many "
            value={this.props.maximum}
            keyboardType="numeric"
            onChangeText={text => this.props.eventUpdate({ prop: 'maximum', value: text })}
          />
        </CardSection>

        <CardSection>
          <Input
            placeholder="Price"
            value={this.props.prix}
            onChangeText={text => this.props.eventUpdate({ prop: 'prix', value: text })}
          />
        </CardSection>
      </View>
    )
  }
}*/
render(){
  return (
    <Form
      ref="testForm"

    >
      <InputField
        ref='first_name'
        label='First Name'
        placeholder='First Name'/>
    </Form>
  )
}
}
const styles= {
  pickerTextStyle: {
    fontSize: 18,
    paddingLeft: 20
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift } = state.eventForm

  return { name, phone, shift }
}

export default connect(mapStateToProps, {eventUpdate}) (EventForm)