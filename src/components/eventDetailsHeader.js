/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Header } from 'react-native-elements'
import BackButton from './backButton'

export default class eventDetailsHeader extends Component {
  render() {
    return (
      <Header
        leftComponent={<BackButton/>}
        centerComponent={{ text: 'DETAILS', style: { color: '#fff' } }}
      />
    );
  }
}
