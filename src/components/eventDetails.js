import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { Button } from 'react-native-elements'
import EventsCardSection from './cardSection'
import EventAttenders from './eventAttenders'
import {eventAttend} from '../actions/eventActions'
import { connect } from 'react-redux'
import firebase from 'firebase'
import Login from './loginForm'

class EventDetails extends Component {
  state = { error: '', attending: false }
  componentWillMount() {
  }

  onAttendPress(){
    // TODO VÉRIFIER SI DEJA ATTENDING
    // TODO si tout est vérifier, incrémenter la variable
    // TODO si déja attending afficher unAttend
    const newAttendance = this.props.event.confirmed + 1
    const max = this.props.event.maximum
    if (newAttendance > max ){
      console.log(this.state.error)
      this.setState({error: 'Le nombre de place maximum a été atteint!'})
    }

    else {
      this.setState({error: '', attending: true})
      this.props.eventAttend({uid: this.props.event.uid, newAttendance})

    }
  }

  isAttending(currentUser) {
    //TODO AVEC LA TABLE DE BD
  }

  canAttend(currentUser) {
    //TODO MAX ATTEINT OU ISATTENDING
  }

  attend(currentUser) {

  }

  renderAttendButton(){
    if(!this.state.attending){
      return (
        <Button
          raised
          onPress={this.onAttendPress.bind(this)}
          icon={{name: 'check'}}
          title='Attend'
          backgroundColor="#2dc448"
        />
      )
    }
    else {
      return <Text></Text>
    }
  }
  renderDeleteButton(){
    const { currentUser } = firebase.auth()
    if(this.props.event.owner === currentUser.email) {
      return (
        <Button
          raised
          onPress={()=>console.log('delete')}
          icon={{name: 'check'}}
          title='Delete'
          backgroundColor="red"
        />
      )
    }
    else return <Text></Text>
  }

  render(){
   const { event } = this.props
    const { currentUser } = firebase.auth()
    if(currentUser) {
      return (
        <View style={styles.containerStyle}>
          <EventsCardSection>
            <View style={styles.headerStyle}>
              <Text style={{fontSize: 20}}><Text style={{fontSize: 20, fontWeight: 'bold'}}>Event: </Text>{event.type}
              </Text>
              <Text style={{fontSize: 20}}><Text style={{fontSize: 20, fontWeight: 'bold'}}>Venue: </Text>{event.place}
              </Text>
              <Text style={{fontSize: 20}}><Text style={{fontSize: 20, fontWeight: 'bold'}}>On: </Text>{event.date}
              </Text>
              <Text style={{fontSize: 20}}><Text style={{fontSize: 20, fontWeight: 'bold'}}>At: </Text>{event.heure}
              </Text>
              <Text style={{fontSize: 20}}><Text
                style={{fontSize: 20, fontWeight: 'bold'}}>Price: </Text>{event.prix || 0}$</Text>
              <Text style={{fontSize: 20}}><Text
                style={{fontSize: 20, fontWeight: 'bold'}}>Attending: </Text>{event.confirmed}</Text>
              <Text style={{fontSize: 20}}><Text
                style={{fontSize: 20, fontWeight: 'bold'}}>Maximum: </Text>{event.maximum}</Text>
              <Text style={{fontSize: 20}}><Text style={{fontSize: 20, fontWeight: 'bold'}}>Contact
                : </Text>{event.owner}</Text>
            </View>
          </EventsCardSection>
          <EventsCardSection>
            <View style={styles.headerStyle}>
              <Text style={{fontSize: 20, padding: 5}}><Text style={{
                fontSize: 20,
                fontWeight: 'bold'
              }}>Description: </Text>{event.description || 'Aucune description'} </Text>
            </View>
          </EventsCardSection>
          <Text style={styles.errorTextStyle}>
            {this.state.error}
          </Text>
          <EventsCardSection>
            <EventAttenders event={this.props.event}/>
          </EventsCardSection>
          {this.renderAttendButton()}
          {this.renderDeleteButton()}
        </View>
      )
    }
    else{
     return(
       <Login/>
     )
    }
  }
}
const styles = {

  containerStyle: {
    backgroundColor: '#fff',
  },
  headerStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    flex: 1
  },
  descriptionStyle: {
    alignItems: 'center',

  },
  attendingStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,

  },
  commentSectionStyle: {

  },
  buttonStyle: {
    width: 100,
    alignSelf: 'center',
    backgroundColor: '#87ff9f',
    margin: 5,
    borderRadius: 5
  },
  buttonTextStyle: {
    alignSelf: 'center',
    color:'#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
}

const mapStateToProps = (state) => {
  const { name, phone, shift } = state.eventForm

  return { name, phone, shift }
}
export default connect(mapStateToProps, {eventAttend})(EventDetails)