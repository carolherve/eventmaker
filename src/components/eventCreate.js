import React, { Component } from 'react'
import Card from './card'
import CardSection from './cardSection'
import Button from './button'
import { eventUpdate, eventCreate } from "../actions/eventActions"
import { connect } from 'react-redux'
import EventForm from './eventForm'



class EventCreate extends Component {

  onButtonPress(){
    const { place, address, pCode, date, heure, type, maximum, prix, description } = this.props
    const confirmed = 1 // initial attending
    this.props.eventCreate({ place, address, pCode, date, heure, type, maximum, confirmed, prix, description })
  }


  render() {

    return (
      <Card>
        <EventForm {...this.props} />

        <CardSection>
          <Button
            text="Create"
            onPress={this.onButtonPress.bind(this)}
          />
        </CardSection>
      </Card>
    )
  }
}



const mapStateToProps= (state) => {
  const { place, address, pCode, date, heure, type, maximum, confirmed, prix, description } = state.eventForm
  return {  place, address, pCode, date, heure, type, maximum, confirmed, prix, description }
}

export default connect(mapStateToProps, { eventUpdate, eventCreate })(EventCreate)