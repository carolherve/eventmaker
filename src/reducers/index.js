import { combineReducers} from 'redux'
import AuthReducer from './authReducer'
import EventFormReducer from './eventFormReducer'
import EventsReducer from './eventsReducer'
import EventAttendencesReducer from './eventsAttendencesReducer'

export default combineReducers({
  auth: AuthReducer,
  eventForm: EventFormReducer,
  events: EventsReducer,
  eventAttendence: EventAttendencesReducer
})