import React from 'react'
import { Scene, Router, Stack } from 'react-native-router-flux'

import EventList from './components/eventsList'
import EventDetails from './components/eventDetails'
import EventCreate from './components/eventCreate'
import Header from './components/eventListHeader'
import EventdetailsHeader from './components/eventDetailsHeader'
import HomeIcon from './components/homeIcon'
import AddIcon from './components/addEventIcon'

const RouterComponent = () => {
  return (
    <Router>
      <Stack key="root">
        <Scene key="tabbar"  duration={1} initial={true}>
          <Scene key="main" tabs>
            <Scene
              key="eventList"
              component={EventList}
              title="All events"
              navBar={Header}
              icon={HomeIcon}
            />
            <Scene
              key="createEvent"
              component={EventCreate}
              title="Create event"
              icon={AddIcon}
              navBar={Header}
            />
          </Scene>
        </Scene>
        <Scene
          key="eventDetails"
          component={EventDetails}
          navBar={EventdetailsHeader}
          tabs={false}
        />
      </Stack>
    </Router>
  )

}

export default RouterComponent