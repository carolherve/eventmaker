import {
  EVENT_PEOPLE_SUCCESS
} from './types'

import firebase from 'firebase'
export const eventsPeople = ({uid}) => {
  return (dispatch) => {
    firebase.database().ref(`/users_events`).orderByChild('event').equalTo(uid)
      .on('value', snapshot => {
        dispatch({ type: EVENT_PEOPLE_SUCCESS, payload: snapshot.val() })
      })
  }
}