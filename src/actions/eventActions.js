import {
  EVENT_UPDATE,
  EVENT_CREATE,
  EVENTS_FETCH_SUCCESS,
  EVENT_SAVE_SUCCESS,
  EVENT_ATTEND,
  EVENT_PEOPLE_SUCCESS
} from './types'

import firebase from 'firebase'
import { Actions } from 'react-native-router-flux'

export const eventUpdate = ({ prop, value }) => {
  return {
    type: EVENT_UPDATE,
    payload: { prop, value }
  }
}

export const eventCreate = ({ place, address, pCode, date, heure, type, maximum, confirmed, prix, description }) => {
  const { currentUser } = firebase.auth()

  return (dispatch) => {
    firebase.database().ref(`/events`)
      .push({ place, address, pCode, date, heure, type, maximum, confirmed, prix, description, 'owner': currentUser.email })
      .then(() => {
        dispatch({ type: EVENT_CREATE })
        Actions.eventList()
  })
  }
}

export const eventsFetch = () => {
  return (dispatch) => {
    firebase.database().ref(`/events`)
      .on('value', snapshot => {
        dispatch({ type: EVENTS_FETCH_SUCCESS, payload: snapshot.val() })
      })
  }
}


export const eventAttend = ({  uid, newAttendance }) => {
  const { currentUser } = firebase.auth()
  return (dispatch) => {
    firebase.database().ref(`/events/${uid}`)
      .update({ confirmed: newAttendance})
      .then(() => {
        firebase.database().ref('/users_events')
          .push({ 'user': currentUser.uid, 'email': currentUser.email, 'event': uid })
          .then(() => {
            dispatch({ type: EVENT_ATTEND })
            Actions.eventList()
        })
      })
  }
}

export const employeeDelete = ({ uid }) => {
  const { currentUser } = firebase.auth()
  return () => {
    firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
      .remove()
      .then(() => {
        Actions.employeeList({ type: 'replace' })
      })
  }
}