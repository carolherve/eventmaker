/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import ReduxThunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reducers from './src/reducers'
import firebase from 'firebase'


import RouterComponent from './src/router'

export default class App extends Component {
  componentWillMount() {
    const config = {
      apiKey: "AIzaSyDiaqwQKvER6H2V0mhsa1Px73A7iBltCqQ",
      authDomain: "eventsmaker-55bb8.firebaseapp.com",
      databaseURL: "https://eventsmaker-55bb8.firebaseio.com",
      projectId: "eventsmaker-55bb8",
      storageBucket: "eventsmaker-55bb8.appspot.com",
      messagingSenderId: "658497571495"

    };
    firebase.initializeApp(config)
  }


  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <RouterComponent />
      </Provider>
    );
  }
}